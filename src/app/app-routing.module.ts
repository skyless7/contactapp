import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainTemplateComponent} from './shared/templates/main-template/main-template.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'contact/list',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MainTemplateComponent,
    children: [
      {
        path: 'contact',
        loadChildren: () => import('./layouts/contact/contact.module').then(mod => mod.ContactModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

