export namespace ContactApp {

    export namespace Interfaces {

        export interface Contact {
            Name: string;
            Surname: string;
            ID?: string;
            Picture: string;
            Favorite: boolean;
            Email: string;
            Numbers: ContactApp.Interfaces.Number[];
        }

        export interface Number {
            Number: string;
            Type: string;
        }

    }

    export namespace Classes {

        export class Number implements ContactApp.Interfaces.Number {
            Number: string;
            Type: string;

            constructor(_Number: string, _Type: string) {
                this.Number = _Number !== null ? _Number : '';
                this.Type = _Type !== null ? _Type : '';
            }
        }

        export class Contact implements ContactApp.Interfaces.Contact {
            Name: string;
            Surname: string;
            Picture: string;
            Favorite: boolean;
            Email: string;
            Numbers: ContactApp.Interfaces.Number[] = [];

            constructor(_Contact: ContactApp.Interfaces.Contact, _Form: any, _Numbers: ContactApp.Interfaces.Number[]) {
                const fullName = _Form.FullName.split(' ');
                if (fullName.length > 1) {
                    this.Name = fullName.length > 1 ? fullName[0] : '';
                    this.Surname = fullName.length > 1 ? fullName[1] : '';
                } else if (fullName.length === 1) {
                    this.Name = fullName[0];
                    this.Surname = '';
                } else {
                    this.Name = '';
                    this.Surname = '';
                }

                this.Picture = _Form.Picture;
                this.Favorite = _Form.Favorite;
                this.Email = _Form.Email;
                this.Numbers = _Numbers;
            }
        }
    }

    export namespace ENUMS {

        export enum ContactFilterTypes {
            ALL = 1,
            FAVORITES = 2,
            CUSTOM = 3
        }

    }

}
