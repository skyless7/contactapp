import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {ConfirmationComponent} from './confirmation/confirmation.component';

@NgModule({
  declarations: [
    ConfirmationComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    ConfirmationComponent
  ],
  providers: [],
  entryComponents: [
    ConfirmationComponent
  ]
})
export class ComponentModule {
}
