import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpService} from '../../../shared/services/http-service';


@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  title: string = '';
  message: string = '';
  action: string = 'Yes';

  constructor(public dialogRef: MatDialogRef<ConfirmationComponent>, private formBuilder: FormBuilder, private httpService: HttpService, @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit(): void {
    this.title = this.data.Title;
    this.message = this.data.Message;
    this.action = this.data.Action;
  }



  // Zatvaranje dialoga
  close(): void {
    this.dialogRef.close(null);
  }

  // Zatvaranje dialoga uz potvrdu
  confirmation(): void {
    this.dialogRef.close(this.data.ID);
  }

}
