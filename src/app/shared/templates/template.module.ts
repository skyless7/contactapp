import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {MainTemplateComponent} from './main-template/main-template.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    HeaderComponent,
    MainTemplateComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    MainTemplateComponent
  ],
  providers: [],
  entryComponents: []
})
export class TemplateModule {
}
