import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {HttpService} from '../../../shared/services/http-service';
import {ContactApp} from '../../../app.dependencies';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
    selector: 'app-contact-details',
    templateUrl: './contact-details.component.html',
    styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit, OnDestroy {

    subscriptions: Subscription[] = [];

    contact: ContactApp.Interfaces.Contact = null;
    contactID: string = null;

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private httpService: HttpService) {
        this.activatedRoute.params.subscribe((params: Params) => {
            this.contactID = params['id'];
        });
    }

    ngOnInit(): void {
        this.contact = JSON.parse(sessionStorage.getItem('Contact'));
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }


    // Postavljanje kontakta da je favorite ili ne
    favoriteContact(_Contact: ContactApp.Interfaces.Contact): void {
        this.httpService.fbUpdate('contacts', _Contact.ID, {
            Name: _Contact.Name,
            Surname: _Contact.Surname,
            Picture: _Contact.Picture,
            Favorite: !_Contact.Favorite
        });

        this.contact.Favorite = !_Contact.Favorite;
    }

    // Navigacija na formu kontakta
    goToForm(): void {
        this.router.navigate(['/contact/form/' + this.contact.ID]);
    }

}
