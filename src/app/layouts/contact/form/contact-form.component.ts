import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpService} from '../../../shared/services/http-service';
import {ContactApp} from '../../../app.dependencies';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
    selector: 'app-contact-form',
    templateUrl: './contact-form.component.html',
    styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit, OnDestroy {

    layoutState: number = 1; // 1 = add 2 = modify

    subscriptions: Subscription[] = [];

    contact: ContactApp.Interfaces.Contact = null;
    contactID: string = null;

    formGroup: FormGroup;
    submitted: boolean = false;

    numbers: ContactApp.Interfaces.Number[] = [{
        Type: '',
        Number: ''
    }];

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private httpService: HttpService) {
        this.formGroup = this.formBuilder.group({
            ID: [null],
            FullName: [null, Validators.required],
            Email: [null],
            Picture: [null],
            Favorite: [false]
        });

        this.activatedRoute.params.subscribe((params: Params) => {
            this.contactID = params['id'];

            this.layoutState = this.contactID === '0' ? 1 : 2;
        });
    }

    ngOnInit(): void {
        if (this.layoutState === 2) {
            this.contact = JSON.parse(sessionStorage.getItem('Contact'));
            this.fillData();
        }

    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }



    // Popunjavanje forme na dohvat podataka
    fillData(): void {
        this.formGroup.controls['ID'].setValue(this.contact.ID);
        this.formGroup.controls['FullName'].setValue(this.contact.Name + ' ' + this.contact.Surname);
        this.formGroup.controls['Email'].setValue(this.contact.Email);
        this.formGroup.controls['Picture'].setValue(this.contact.Picture);
        this.formGroup.controls['Favorite'].setValue(this.contact.Favorite);
        this.numbers = this.contact.Numbers;
    }

    // Spremanje ili kreranje zapisa u bazu podataka
    save(): void {
        this.submitted = true;

        if (this.formGroup.invalid) {
            return;
        }

        if (this.layoutState === 1) {
            this.httpService.fbCreate('contacts', new ContactApp.Classes.Contact(this.contact, this.formGroup.value, this.numbers));
        } else {
            this.httpService.fbUpdate('contacts', this.contactID, new ContactApp.Classes.Contact(this.contact, this.formGroup.value, this.numbers));
        }

        setTimeout(() => {
            this.router.navigate(['/contact/list']);
        }, 500);


    }

    // Brisanje broja
    removeNumber(_Number: ContactApp.Interfaces.Number): void {
        this.numbers.splice(this.numbers.indexOf(_Number), 1);
    }

    // Dodavanje novog broja
    addNewNumber(): void {
        this.numbers.push(new ContactApp.Classes.Number(null, null));
    }

    get f(): any { return this.formGroup.controls; }

}
