import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {ContactListComponent, InitialsPipe} from './list/contact-list.component';
import {ContactDetailsComponent} from './details/contact-details.component';
import {ContactFormComponent} from './form/contact-form.component';
import {ContactRoutingModule} from './contact.routing.module';
import {ComponentModule} from '../../shared/components/component.module';

@NgModule({
    declarations: [
        ContactListComponent,
        ContactDetailsComponent,
        ContactFormComponent,
        InitialsPipe
    ],
    imports: [
        CommonModule,
        ContactRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        ComponentModule
    ],
    providers: [],
    entryComponents: []
})
export class ContactModule {
}
