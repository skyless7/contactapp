import {Component, OnDestroy, OnInit, Pipe, PipeTransform} from '@angular/core';
import {Subscription} from 'rxjs';
import {HttpService} from '../../../shared/services/http-service';
import {ContactApp} from '../../../app.dependencies';
import {ConfirmationComponent} from '../../../shared/components/confirmation/confirmation.component';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';

@Component({
    selector: 'app-contact-list',
    templateUrl: './contact-list.component.html',
    styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit, OnDestroy {

    subscriptions: Subscription[] = [];
    contactList: ContactApp.Interfaces.Contact[] = [];
    fullContactList: ContactApp.Interfaces.Contact[] = [];

    filterState: number = 1;
    filter: string = '';

    constructor(private httpService: HttpService, private dialog: MatDialog, private router: Router) {
    }

    ngOnInit(): void {
        this.getContactList();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }


    // Dohvačanje liste kontakata iz baze
    getContactList(): void {
        this.subscriptions.push(this.httpService.fbGet('contacts').subscribe(data => {
                this.fullContactList = data.map(e => {
                    return {
                        ID: e.payload.doc.id,
                        Name: e.payload.doc.data()['Name'],
                        Surname: e.payload.doc.data()['Surname'],
                        Picture: e.payload.doc.data()['Picture'],
                        Favorite: e.payload.doc.data()['Favorite'],
                        Email: e.payload.doc.data()['Email'],
                        Numbers: e.payload.doc.data()['Numbers']
                    } as ContactApp.Interfaces.Contact;
                });
                this.contactList = JSON.parse(JSON.stringify(this.fullContactList));
            },
            error => {
                console.log('error');
            }));
    }

    // Brisanje kontakta
    removeContact(_ContactID: string, _Event: any): void {
        _Event.stopPropagation();

        const dialogRef = this.dialog.open(ConfirmationComponent, {
            width: '400px',
            data: {Title: 'Delete', Message: 'Are you sure you want to delete this contact?', Action: 'Delete', ID: _ContactID}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (!result) {
                return;
            }
            this.httpService.fbDelete('contacts', _ContactID);
        });
    }

    // Postavljanje kontakta da je favorite ili ne
    favoriteContact(_Contact: ContactApp.Interfaces.Contact, _Event: any): void {
        _Event.stopPropagation();

        this.httpService.fbUpdate('contacts', _Contact.ID, {
            Name: _Contact.Name,
            Surname: _Contact.Surname,
            Picture: _Contact.Picture,
            Favorite: !_Contact.Favorite
        });
    }

    // Filtriranje liste kontakata
    filterContacts(_State: ContactApp.ENUMS.ContactFilterTypes): void {

        if (_State) {
            this.filterState = _State;
        }

        switch (this.filterState) {
            case 1: {
                this.contactList = this.fullContactList;
                break;
            }
            case 2: {
                this.contactList = this.fullContactList.filter((contact: ContactApp.Interfaces.Contact) => {
                    return contact.Favorite === true;
                });
                break;
            }
            default: {
                this.contactList = this.fullContactList;
                break;
            }
        }
    }

    // Filtriranje po ključnoj rijeći
    customFilter(_Event: any): void {
        this.filterContacts(null);

        this.filter = this.filter.toLowerCase();
        const searchText = this.filter.split(' ');

        searchText.forEach((search: string) => {
            this.contactList = this.contactList.filter((contact: ContactApp.Interfaces.Contact) => {
                if (contact.Name && contact.Surname) {
                    return (contact.Name + ' ' + contact.Surname).toLowerCase().includes(search);
                }
            });
        });
    }

    // Navigacija na detalje kontakta
    goToDetails(_Contact: ContactApp.Interfaces.Contact): void {
        sessionStorage.setItem('Contact', JSON.stringify(_Contact));
        this.router.navigate(['/contact/details/' + _Contact.ID]);
    }

    // Navigacija na formu kontakta
    goToForm(_Contact: ContactApp.Interfaces.Contact): void {
        sessionStorage.setItem('Contact', JSON.stringify(_Contact));
        this.router.navigate(['/contact/form/' + _Contact.ID]);
    }

}

@Pipe({
    name: 'returnInitials',
    pure: true
})

export class InitialsPipe implements PipeTransform {

    constructor() { }

    transform(_Name: string, _Surname: string): any {
        if (!_Name && !_Surname) {
            return '';
        }

        return _Name.charAt(0) + _Surname.charAt(0);
    }
}
