import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {ContactListComponent} from './list/contact-list.component';
import {ContactDetailsComponent} from './details/contact-details.component';
import {ContactFormComponent} from './form/contact-form.component';

const routes: Routes = [
  {
    path: 'list',
    component: ContactListComponent
  },
  {
    path: 'details/:id',
    component: ContactDetailsComponent
  },
  {
    path: 'form/:id',
    component: ContactFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRoutingModule {
}
