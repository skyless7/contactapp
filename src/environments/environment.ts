// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBfKiza4QV-s8EQ5setbixq8Tqj6-Ae0-0',
    authDomain: 'contact-app-b1ffe.firebaseapp.com',
    databaseURL: 'https://contact-app-b1ffe.firebaseio.com',
    projectId: 'contact-app-b1ffe',
    storageBucket: 'contact-app-b1ffe.appspot.com',
    messagingSenderId: '1070146109559',
    appId: '1:1070146109559:web:3816d5554e492065c84672'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
